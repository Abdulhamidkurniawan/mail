<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuratkeluarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suratkeluars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_urut')->nullable();
            $table->string('no_surat')->nullable();
            $table->string('nama')->nullable();
            $table->string('hp')->nullable();
            $table->string('hal')->nullable();
            $table->string('dari_instansi')->nullable();
            $table->string('dari_pengirim')->nullable();
            $table->string('dari_alamat')->nullable();
            $table->string('tujuan_instansi')->nullable();
            $table->string('tujuan_pengirim')->nullable();
            $table->string('tujuan_alamat')->nullable();
            $table->text('isi', "2000")->nullable();
            $table->string('petugas')->nullable();
            $table->string('tembusan')->nullable();
            $table->string('file')->nullable();
            $table->string('tanggal')->nullable();
            $table->string('tanggal_surat')->nullable();
            $table->string('status')->nullable();
            $table->string('catatan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suratkeluars');
    }
}
