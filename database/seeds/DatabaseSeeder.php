<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['name' => 'admin',
            'username' => 'admin',
            'email' => 'admin@gmail.com',
            'jabatan' => 'admin',
            'password' => bcrypt('samarinda')],
            ['name' => 'karyawan',
            'username' => 'karyawan',
            'email' => 'karyawan@gmail.com',
            'jabatan' => 'karyawan',
            'password' => bcrypt('samarinda')],
            ['name' => 'user',
            'username' => 'user',
            'email' => 'user@gmail.com',
            'jabatan' => 'user',
            'password' => bcrypt('samarinda')],
        ]);

        DB::table('jabatans')->insert([
            ['jabatan' => 'admin'],
            ['jabatan' => 'karyawan'],
            ['jabatan' => 'user'],
        ]);

        DB::table('works')->insert([
            ['nama' => 'F Kaisan',
            'nik' => '199002237801016',
            'instansi' => 'Kecamanatan Samarinda Ulu',
            'alamat' => 'Jl. Ir. H. Juanda No.5, Air Putih, Kec. Samarinda Ulu, Kota Samarinda, Kalimantan Timur 75124',
            'jabatan' => 'Kasubag Keuangan',
            'bagian' => 'Keuangan',
            'subbagian' => '',
            'golongan' => 'IIIB',],
        ]);
    }
}
