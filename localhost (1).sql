-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Okt 31, 2020 at 02:11 AM
-- Server version: 5.7.24
-- PHP Version: 7.3.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db`
--
CREATE DATABASE IF NOT EXISTS `db` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `db`;

-- --------------------------------------------------------

--
-- Table structure for table `debits`
--

CREATE TABLE `debits` (
  `id` int(10) UNSIGNED NOT NULL,
  `debit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `response` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jabatans`
--

CREATE TABLE `jabatans` (
  `id` int(10) UNSIGNED NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jabatans`
--

INSERT INTO `jabatans` (`id`, `jabatan`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL),
(2, 'pegawai', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_11_12_154812_create_debits_table', 1),
(5, '2019_11_12_162907_create_pumps_table', 1),
(6, '2019_11_13_160912_create_jabatans_table', 1),
(7, '2019_11_13_160955_create_waters_table', 1),
(8, '2020_03_30_135819_create_monitors_table', 1),
(9, '2020_05_12_144157_create_powers_table', 1),
(10, '2020_10_09_093727_create_surats_table', 2),
(11, '2020_10_09_095008_create_pasiens_table', 2),
(12, '2020_10_09_095248_create_rms_table', 3),
(13, '2020_10_15_113502_create_works_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `monitors`
--

CREATE TABLE `monitors` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_node` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve9` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pasiens`
--

CREATE TABLE `pasiens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ktp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ttl` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jk` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pekerjaan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pasiens`
--

INSERT INTO `pasiens` (`id`, `nama`, `ktp`, `ttl`, `jk`, `pekerjaan`, `alamat`, `hp`, `created_at`, `updated_at`) VALUES
(1, 'hamid', '6472032802930002', 'Samarinda,01 Agustus 2020', 'LAKI-LAKI', 'Swasta', 'Jl. Wiraswasrt, RT.13, No.6 Kel.Sidodadi, Kec.Samarinda ULU KOTA SAMARINDA', '083722222222', NULL, '2020-10-12 16:20:17'),
(2, 'Oktavia Tri Wulandari', '6324235646456456', '18 Januari 2024', 'PEREMPUAN', 'PNS', 'sssssssss sssssssss sssssssss sssssssss sssssssss sssssssss sssssssss sssssssss sssssssss sssssssss sssssssss sssssssss sssssssss', '0875222277777', '2020-10-15 18:38:42', '2020-10-19 03:56:16');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `powers`
--

CREATE TABLE `powers` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_node` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_message` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fasa_r` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fasa_s` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fasa_t` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiga_fasa` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pumps`
--

CREATE TABLE `pumps` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `response` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rms`
--

CREATE TABLE `rms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ktp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ttl` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_rm` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hasil` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rms`
--

INSERT INTO `rms` (`id`, `nama`, `ktp`, `ttl`, `jenis_rm`, `hasil`, `tanggal`, `created_at`, `updated_at`) VALUES
(1, 'Oktavia Tri Wulandaria', '6324235646456456', 'Samarinda, 26 Oktober 1993a', 'Swab Test IFA', 'REAKTIF', '06 Oktober 2020', '2020-10-12 23:31:49', '2020-10-12 23:31:49'),
(2, 'Oktavia Tri Wulandaria', '6324235646456456', 'Samarinda, 26 Oktober 1993a', 'Swab Test IFA', 'NON REAKTIF', '06 Oktober 2020', '2020-10-12 23:32:36', '2020-10-12 23:32:36'),
(3, 'Oktavia Tri Wulandaria', '6324235646456456', 'Samarinda, 26 Oktober 1993a', 'Swab Test IFA', 'Positif', '30 Oktober 2020', '2020-10-12 23:40:12', '2020-10-12 23:40:12'),
(4, 'Oktavia Tri Wulandaria', '6324235646456456', 'Samarinda, 26 Oktober 1993a', 'Swab Test IFA', 'Positif', '30 Oktober 2020', '2020-10-12 23:40:56', '2020-10-12 23:40:56'),
(5, 'Oktavia Tri Wulandaria', '6324235646456456', 'Samarinda, 26 Oktober 1993a', 'Swab Test IFA', 'Positif', '30 Oktober 2020', '2020-10-12 23:41:09', '2020-10-12 23:41:09'),
(6, 'Oktavia Tri Wulandaria', '6324235646456456', 'Samarinda, 26 Oktober 1993a', 'Swab Test IFA', 'Negatif', '30 Oktober 2020', '2020-10-12 23:41:20', '2020-10-12 23:41:20'),
(8, 'hamid', '6472032802930002', 'Samarinda,01 Agustus 2020', 'Swab Test Antigen', 'REAKTIF', '22 Oktober 2020', '2020-10-13 00:41:26', '2020-10-13 00:41:26'),
(9, 'hamid', '6472032802930002', 'Samarinda,01 Agustus 2020', 'Rapid Test', 'REAKTIF', '13 Oktober 2020', '2020-10-13 04:11:57', '2020-10-13 04:11:57'),
(10, 'Oktavia Tri Wulandaria', '6324235646456456', 'Samarinda, 26 Oktober 1993a', 'Rapid Test', 'REAKTIF', '15 Oktober 2020', '2020-10-14 22:32:53', '2020-10-14 22:32:53'),
(11, 'hamid', '6472032802930002', 'Samarinda,01 Agustus 2020', 'Rapid Test', 'REAKTIF', '15 Oktober 2020', '2020-10-15 01:22:08', '2020-10-15 01:22:08'),
(12, 'hamid', '6472032802930002', 'Samarinda,01 Agustus 2020', 'Swab Test Antigen', 'Positif', '29 Oktober 2020', '2020-10-15 03:12:41', '2020-10-15 03:12:41');

-- --------------------------------------------------------

--
-- Table structure for table `surats`
--

CREATE TABLE `surats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_urut` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_surat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ktp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ttl` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jk` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pekerjaan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_rm` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hasil` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_rm` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipe_pasien` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dokter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `surats`
--

INSERT INTO `surats` (`id`, `no_urut`, `no_surat`, `nama`, `ktp`, `ttl`, `jk`, `pekerjaan`, `alamat`, `jenis_rm`, `hasil`, `no_rm`, `tipe_pasien`, `dokter`, `tanggal`, `created_at`, `updated_at`) VALUES
(1, '00001', '1/KBB-SK/X/2020', 'Oktavia Tri Wulandari', '6324235646456456', NULL, NULL, NULL, NULL, 'Rapid Test', 'REAKTIF', 'KBB-SK/201020/1', NULL, NULL, '19 Oktober 2020', '2020-10-19 03:27:42', '2020-10-20 03:27:42'),
(2, '00001', '2/KBB-SK/X/2020', 'Oktavia Tri Wulandari', '6324235646456456', NULL, NULL, NULL, NULL, 'Rapid Test', 'REAKTIF', 'KBB-SK/201020/2', NULL, NULL, '18 Oktober 2020', '2020-10-18 03:27:42', '2020-10-19 03:27:42'),
(6, '00003', '3/KBB-SK/X/2020', 'Oktavia Tri Wulandari', '6324235646456456', NULL, NULL, NULL, NULL, 'IFA', 'Positif', 'KBB-SK/201020/3', NULL, NULL, '13 Oktober 2020', '2020-10-19 08:23:14', '2020-10-20 08:23:14'),
(9, '00004', '4/KBB-SK/X/2020', 'Oktavia Tri Wulandari', '6324235646456456', NULL, NULL, NULL, NULL, 'Rapid Test', 'REAKTIF', 'KBB-SK/201020/4', NULL, NULL, '20 Oktober 2020', '2020-10-20 08:54:59', '2020-10-20 08:54:59'),
(10, '00005', '5/KBB-SK/X/2020', 'hamid', '6472032802930002', NULL, NULL, NULL, NULL, 'Rapid Test', 'REAKTIF', 'KBB-SK/201020/5', NULL, NULL, '20 Oktober 2020', '2020-10-20 10:07:29', '2020-10-20 10:07:29'),
(11, '00006', '6/KBB-SK/X/2020', 'Oktavia Tri Wulandari', '6324235646456456', NULL, NULL, NULL, NULL, 'Swab Test Antigen', 'Positif', 'KBB-SK/201020/6', NULL, NULL, '23 Oktober 2020', '2020-10-20 10:15:20', '2020-10-20 10:15:20'),
(12, '00007', '7/KBB-SK/X/2020', 'Oktavia Tri Wulandari', '6324235646456456', NULL, NULL, NULL, NULL, 'IFA', 'Positif', 'KBB-SK/201020/7', NULL, NULL, '13 Oktober 2020', '2020-10-20 10:16:11', '2020-10-20 10:16:11'),
(13, '00008', '8/KBB-SK/X/2020', 'Oktavia Tri Wulandari', '6324235646456456', NULL, NULL, NULL, NULL, 'Rapid Test', 'REAKTIF', 'KBB-SK/211020/8', NULL, NULL, '21 Oktober 2020', '2020-10-20 19:08:12', '2020-10-20 19:08:12');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `jabatan`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', NULL, '$2y$10$H4waGwZJWLx0IaqOOxKMreMAMwNYLKftmZnEiNFkBVFK8mzKOlwBm', 'admin', NULL, '2020-10-09 01:34:01', '2020-10-09 21:57:45'),
(3, 'sama', 'sama@gmail.com', NULL, '$2y$10$AVIjY.QznN1.jUL3WFRl/.xlPeKoxxCbUBteLo7U49ZESRYMTG4Q.', 'pegawai', NULL, '2020-10-09 21:56:20', '2020-10-09 21:56:54');

-- --------------------------------------------------------

--
-- Table structure for table `waters`
--

CREATE TABLE `waters` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_node` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_message` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `battery` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valve` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `consumption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `works`
--

CREATE TABLE `works` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pekerjaan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `works`
--

INSERT INTO `works` (`id`, `pekerjaan`, `created_at`, `updated_at`) VALUES
(1, 'PNS', '2020-10-15 07:40:53', '2020-10-15 07:40:53'),
(2, 'Karyawan Swasta', '2020-10-15 08:02:52', '2020-10-15 08:02:52'),
(3, 'Wiraswasta', '2020-10-17 04:47:38', '2020-10-17 04:47:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `debits`
--
ALTER TABLE `debits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jabatans`
--
ALTER TABLE `jabatans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `monitors`
--
ALTER TABLE `monitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pasiens`
--
ALTER TABLE `pasiens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `powers`
--
ALTER TABLE `powers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pumps`
--
ALTER TABLE `pumps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rms`
--
ALTER TABLE `rms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surats`
--
ALTER TABLE `surats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `waters`
--
ALTER TABLE `waters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `works`
--
ALTER TABLE `works`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `debits`
--
ALTER TABLE `debits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jabatans`
--
ALTER TABLE `jabatans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `monitors`
--
ALTER TABLE `monitors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pasiens`
--
ALTER TABLE `pasiens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `powers`
--
ALTER TABLE `powers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pumps`
--
ALTER TABLE `pumps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rms`
--
ALTER TABLE `rms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `surats`
--
ALTER TABLE `surats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `waters`
--
ALTER TABLE `waters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `works`
--
ALTER TABLE `works`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
