<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Auth::routes();

// Route::middleware('cors')->group(function() {
  // Route::post('/loginapps', function(){ return 'sukses'; });
// });
Route::post('/loginapps', 'Auth\LoginController@login')->name('loginapps');
Route::post('/logoutapps', 'Auth\LoginController@logout')->name('logoutapps');

Route::get('/', 'RedirectController@default')->name('default');
Route::get('/cari', 'RedirectController@cari')->name('cari');
Route::post('/cari', 'RedirectController@cek')->name('cek');
Route::get('/hasil', 'RedirectController@hasil')->name('hasil');
Route::get('/getnama/{id}','SuratKeluarController@getnama')->name('getnama');
Route::get('/getalamat/{id}','SuratKeluarController@getalamat')->name('getalamat');

Route::middleware('auth')->group(function() {

Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
Route::get('/optimize', 'HomeController@optimize')->name('optimize');

Route::get('/sk', 'SuratKeluarController@index')->name('sk.index');
Route::get('/sk/create', 'SuratKeluarController@create')->name('sk.create');
Route::post('/sk/create', 'SuratKeluarController@store')->name('sk.store');
Route::get('/sk/{sk}/edit', 'SuratKeluarController@edit')->name('sk.edit');
Route::patch('/sk/{sk}/edit', 'SuratKeluarController@update')->name('sk.update');
Route::delete('/sk/{sk}/delete', 'SuratKeluarController@destroy')->name('sk.destroy');

Route::get('/sm', 'SuratMasukController@index')->name('sm.index');
Route::post('/sm/create', 'SuratMasukController@store')->name('sm.store');
Route::get('/sm/{sk}/show', 'SuratMasukController@show')->name('sm.show');
Route::get('/sm/{sk}/forward', 'SuratMasukController@forward')->name('sm.forward');
Route::get('/sm/{sk}/selesai', 'SuratMasukController@selesai')->name('sm.selesai');
Route::get('/sm/{sk}/diarsipkan', 'SuratMasukController@diarsipkan')->name('sm.diarsipkan');
Route::delete('/sm/{sm}/delete', 'SuratMasukController@destroy')->name('sm.destroy');
Route::post('/report', 'RedirectController@report')->name('report');

Route::middleware('admin')->group(function() {
    // Ruoute akun user
    Route::get('/user_panel', 'UserPanelController@index')->name('user_panel.index');
    Route::get('/user_panel/create', 'UserPanelController@create')->name('user_panel.create');
    Route::post('/user_panel/create', 'UserPanelController@store')->name('user_panel.store');
    // Route::get('/user_panel/{post}', 'UserPanelController@show')->name('user_panel.show');
    Route::get('/user_panel/{user}/edit', 'UserPanelController@edit')->name('user_panel.edit');
    Route::patch('/user_panel/{user}/edit', 'UserPanelController@update')->name('user_panel.update');
    Route::delete('/user_panel/{user}/delete', 'UserPanelController@destroy')->name('user_panel.destroy');

    Route::post('/pekerjaan/create', 'WorksController@store')->name('pekerjaan.store');
    Route::delete('/pekerjaan/delete', 'WorksController@destroy')->name('pekerjaan.destroy');

    Route::get('/test', 'TestController@index')->name('test.index');
    Route::post('/test/create', 'TestController@store')->name('test.store');
    Route::delete('/test/delete', 'TestController@destroy')->name('test.destroy');

    });});
