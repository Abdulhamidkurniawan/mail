@extends('layouts.dashboard')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Surat Keluar</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">

<div class="box">
            <div class="box-header">
              <h3 class="box-title">Surat Keluar</h3>
            </div>

            <div class="box-body">
            <a href="{{ route('sk.create') }}" class="btn btn-sm btn-primary">Buat Surat</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No Surat</th>
                  <th>Hal</th>
                  <th>Dari</th>
                  <th>Tujuan</th>
                  <th>Tanggal Surat</th>
                  <th>Status</th>
                  <th>File</th>
                  {{-- <th>Aksi</th> --}}
                </tr>
                </thead>
                <tbody>
                    @foreach ($sks as $sk)
                    {{-- @if ($sk->status == 'Terkirim') --}}
                    <tr>
                        <td>{{$sk->no_surat}}</td>
                        <td>{{$sk->hal}}</td>
                        <td>{{$sk->dari_instansi.'-'.$sk->dari_pengirim}}</td>
                        <td>{{$sk->tujuan_instansi.'-'.$sk->tujuan_pengirim}}</td>
                        <td>{{$sk->tanggal_surat}}</td>
                        <td>{{$sk->status}}</td>
                        {{-- <td align="center"><a href="{{ URL::asset('upload_surat/'.$sk->file) }}" class="fa fa-download" target="_blank" title="Download File"></a></td> --}}
                        <td width=100 align=center>
                        {{-- <form action="{{ route('sk.destroy', $sk) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }} --}}
                        <a href="{{ URL::asset('upload_surat/'.$sk->file) }}" class="fa fa-download" target="_blank" title="Download File">&nbsp;</a>
                        {{-- <a onclick="return confirm('Hapus data surat?')" type="submit" class="fa fa-close" target="_blank" title="Hapus">&nbsp;</a> --}}
                        {{-- <a href="javascript:;" onclick="parentNode.submit();" class="fa fa-close" title="Hapus">&nbsp;</a>
                        </form> --}}
                        </td>
                </tr>
                {{-- @endif --}}
                @endforeach

                </tbody>

              </table>
            </div>
            <!-- /.box-body -->
          </div>
        <!-- right col -->
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2014-2020 <a href="#">BBDSG</a>
  </footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="{{asset('bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{asset('bower_components/morris.js/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
    $('#example1').dataTable( {
      'aaSorting': []
  } );
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

@endsection
