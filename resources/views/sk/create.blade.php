@extends('layouts.dashboard')
@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>
<script src="{{asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js')}}"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
<script>
$.noConflict();
jQuery( document ).ready(function( $ ) {
  $( function() {
  $( "#datepicker" ).datepicker({
    format: 'yyyy-mm-dd',
    changeMonth: true,
    changeYear: true});
} );
});
// Code that uses other library's $ can follow here.
</script>
<!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
<script src="{{asset('js/jquery-dropdown-datepicker.min.js')}}"></script> -->
<script>
$.noConflict();
jQuery( document ).ready(function( $ ) {
$("#date").dropdownDatepicker({
    defaultDateFormat: 'yyyy-mm-dd',
    displayFormat: 'dmy',
    minYear: 1940,
    allowFuture:false,
    submitFormat: 'yyyy-mm-dd'
});
});
</script>

<script>
function alphaOnly(event) {
  var key = event.keyCode;
  return ((key >= 65 && key <= 90) || key == 8 || key == 32|| key == 37|| key == 38|| key == 39|| key == 40|| key == 46);
};
</script>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        {{-- <small>Control panel</small> --}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">

    <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Surat</h3>
            </div>

        <div class="box-body">

                <div class="card-body">
                    <form method="POST" action="{{ route('sk.store') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="no_surat" class="col-md-1 col-form-label text-md-right">Nomor Surat</label>

                            <div class="col-md-4">
                                <input id="no_surat" oninput="let p=this.selectionStart;this.value=this.value.toUpperCase();this.setSelectionRange(p, p);" type="text" class="form-control" name="no_surat" value="{{ old('no_surat') }}" required autofocus>
                            </div>
                            @if ($errors->has('no_surat'))
                            <span class="invalid-feedback" role="alert">
                                <strong style='color:red;'>{{ $errors->first('no_surat') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label for="usage" class="col-md-1 col-form-label text-md-right">No HP</label>

                            <div class="col-md-4">
                                <input type="number" id="hp" type="text" class="form-control" name="hp" value="{{ old('hp') }}" placeholder="08xx" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                        <label for="dari"class="col-md-1 col-form-label text-md-right">Dari</label>
                            <div class="col-md-3">
                                <select name="dari_instansi" id="dari_instansi" class="form-control select2">
                                    <option value="">Pilih Instansi</option>
                                    @foreach ($works as $work)
                                        <option value="{{$work->instansi}}">{{$work->instansi}}</option>
                                    @endforeach
                                </select>
                            </div>
                                <button type="button" class="btn btn-success float-right mb-1" data-toggle="modal" data-target="#modalTambahBarang">Tambah / Hapus</button>
                        </div>

                        <div class="form-group row">
                            <label for="dari"class="col-md-1 col-form-label text-md-right">Pengirim</label>
                                <div class="col-md-4">
                                    <select name="dari_pengirim" id="dari_pengirim" class="form-control select2">
                                        <option value="">Pilih Pengirim</option>
                                    </select>
                                </div>
                        </div>

                        <div class="form-group row">
                            <label for="dari"class="col-md-1 col-form-label text-md-right">Alamat Pengirim</label>
                                <div class="col-md-4">
                                    <select name="dari_alamat" id="dari_alamat" class="form-control select2">
                                    </select>
                                </div>
                        </div>

                        {{-- <div class="form-group row">
                            <label for="usage" class="col-md-1 col-form-label text-md-right">Penjual</label>

                            <div class="col-md-4">
                                <input id="penjual" type="text" class="form-control" name="penjual" value="{{ Auth::user()->name }}" readonly required>
                            </div>
                        </div> --}}
                        <div class="form-group row">
                            <label for="usage" class="col-md-1 col-form-label text-md-right">Hal</label>

                            <div class="col-md-4">
                                <input id="hal" type="text" class="form-control" name="hal" value="{{ old('hal') }}" required>
                            </div>
                        </div>

                        {{-- <div class="form-group row">
                            <label for="alamat" class="col-md-1 col-form-label text-md-right">Alamat</label>

                            <div class="col-md-4">
                            <textarea name="alamat" id="alamat" class="form-control" placeholder="Alamat">{{ old('alamat') }}</textarea>
                            </div>
                        </div> --}}

                        <div class="form-group row">
                            <label for="dari"class="col-md-1 col-form-label text-md-right">Tujuan Instansi</label>
                                <div class="col-md-4">
                                    <select name="tujuan_instansi" id="tujuan_instansi" class="form-control select2">
                                        <option value="">Pilih Instansi</option>
                                        @foreach ($works as $work)
                                            <option value="{{$work->instansi}}">{{$work->instansi}}</option>
                                        @endforeach
                                            </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="dari"class="col-md-1 col-form-label text-md-right">Penerima</label>
                                    <div class="col-md-4">
                                        <select name="tujuan_pengirim" id="tujuan_pengirim" class="form-control select2">
                                            <option value="">Pilih Penerima</option>
                                        </select>
                                    </div>
                            </div>

                            <div class="form-group row">
                                <label for="dari"class="col-md-1 col-form-label text-md-right">Alamat Penerima</label>
                                    <div class="col-md-4">
                                        <select name="tujuan_alamat" id="tujuan_alamat" class="form-control select2">
                                        </select>
                                    </div>
                            </div>

                        <div class="form-group row">
                            <label for="location" class="col-md-1 col-form-label text-md-right">Isi/Ringkasan</label>

                            <div class="col-md-4">
                                <textarea id="isi" type="text" class="form-control" name="isi" value="{{ old('isi') }}" rows="5" cols="40"></textarea>

                            </div>
                        </div>

                        {{-- <div class="form-group row">
                            <label for="sender" class="col-md-4 col-form-label text-md-right">Promosi</label>

                            <div class="col-md-6">
                                <input id="promosi" type="text" class="form-control" name="promosi" value="{{ old('promosi') }}" autofocus>
                            </div>
                        </div> --}}

                        <div class="form-group row">
                            <label for="tanggal" class="col-md-1 col-form-label text-md-right">Tanggal Surat</label>

                            <div class="col-md-4">
                                <input autocomplete="off" id="datepicker" type="text" class="form-control" name="tanggal" value="{{ old('tanggal') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                        <label for="response" class="col-md-1 col-form-label text-md-right">Unggah Surat</label>

                        <div class="col-md-4">
                            <input type="file" name="file" class="form-control" placeholder="pdf/doc/docx">
                        </div>
                </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Kirim Surat') }}
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="modal fade" id="modalTambahBarang" tabindex="-1" aria-labelledby="modalTambahBarang" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Tambah Pengirim / Penerima Baru</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                <!--FORM TAMBAH BARANG-->
                                <form method="POST" action="{{ route('pekerjaan.store') }}">
                                @csrf
                                <div class="form-group row">
                                    <label for="tker" class="col-md-2 col-form-label text-md-right">Biodata</label>
                                        <div class="col-md-6">
                                            <input id="tinstansi" type="text" class="form-control" name="tinstansi" value="" placeholder="Nama Instansi Baru" autofocus>
                                        </div>
                                </div>
                                {{-- <div class="form-group row">
                                    <label for="tker" class="col-md-2 col-form-label text-md-right"></label>
                                        <div class="col-md-6">
                                            <input id="tnama" type="text" class="form-control" name="tnama" value="" placeholder="Nama">
                                        </div>
                                </div>
                                <div class="form-group row">
                                    <label for="tker" class="col-md-2 col-form-label text-md-right"></label>
                                        <div class="col-md-6">
                                            <input id="tnip" type="text" class="form-control" name="tnip" value="" placeholder="NIP">
                                        </div>
                                </div> --}}
                                <div class="form-group row">
                                    <label for="tker" class="col-md-2 col-form-label text-md-right"></label>
                                        <div class="col-md-6">
                                            <textarea name="talamat" id="talamat" class="form-control" placeholder="Alamat">{{ old('talamat') }}</textarea>
                                        </div>
                                    <button type="submit" class="btn btn-primary">Tambah</button>
                                    </form>
                                </div>
                            </div>
                            <div class="modal-body">
                                <!--FORM TAMBAH BARANG-->
                                    <div class="form-group row">
                                        <label for="tker" class="col-md-2 col-form-label text-md-right">Ingin dihapus</label>
                                        <div class="col-md-6">
                                            <select name="" onchange="copier(event)" class="form-control select2">
                                                <option value="">Pilih Data</option>
                                            @foreach ($datas as $data)
                                                <option value="{{$data->id}}">{{$data->instansi}}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                        @if (!$datas->isEmpty())
                                        <form method="POST" action="{{ route('pekerjaan.destroy', $data->id) }}">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <input id="kker" name="kker" type="hidden" value="">

                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                        {{-- <button type="button" onclick="myFunction()">Try it</button> --}}
                                        </form>
                                        @endif
                                </div>
                                </div>
                            </div>

                        <!--END FORM TAMBAH BARANG-->
                        </div>
                        </div>

                </div>

    </div>
            <!-- /.box-body -->
    </div>
        <!-- right col -->
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="#">BBDSG</a>
  </footer>

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="{{asset('bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{asset('bower_components/morris.js/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

<script type="text/javascript">
    $(document).ready(function() {
      $(".btn-success").click(function(){
          var html = $(".clone").html();
          $(".increment").after(html);
      });
      $("body").on("click",".btn-danger",function(){
          $(this).parents(".control-group").remove();
      });
    });
</script>

<!-- CK Editor -->
<script src="{{asset('bower_components/ckeditor/ckeditor.js')}}"></script>
<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('isi')
      //bootstrap WYSIHTML5 - text editor
      $('.textarea').wysihtml5()
    })
  </script>
<script type="text/javascript">
    jQuery(document).ready(function ()
    {
                jQuery('select[name="dari_instansi"]').on('change',function(){
                   var id = jQuery(this).val();
                   console.log(id)

                //    alert(countryID);
                   if(id)
                   {
                      jQuery.ajax({
                         url : 'https://'+document.location.hostname+'/mail/public/getnama/'+id,
                         type : "GET",
                         dataType : "json",
                         cache: false,
                         success:function(data)
                         {
                            // alert(data);
                            console.log(data);
                            jQuery('select[name="dari_pengirim"]').empty();
                            $('select[name="dari_pengirim"]').append('<option value="'+ "" +'">'+ "Pilih Pengirim" +'</option>');
                            jQuery.each(data, function(nama,alamat){
                                // console.log(nama);
                                // console.log(alamat);
                                $('select[name="dari_pengirim"]').append('<option value="'+ nama +'">'+ alamat + ' - ' + nama +'</option>');
                          });
                         }
                      });
                   }
                   else
                   {
                      $('select[name="dari_pengirim"]').empty();
                   }
                });

                jQuery('select[name="dari_instansi"]').on('change',function(){
                   var id = jQuery(this).val();
                   console.log(id)

                //    alert(id);
                   if(id)
                   {
                      jQuery.ajax({
                         url : 'https://'+document.location.hostname+'/mail/public/getalamat/'+id,
                         type : "GET",
                         dataType : "json",
                         cache: false,
                         success:function(data)
                         {
                            // alert(data);
                            console.log(data);
                            jQuery('select[name="dari_alamat"]').empty();
                            jQuery.each(data, function(nama,alamat){
                                // console.log(nama);
                                // console.log(alamat);
                                $('select[name="dari_alamat"]').append('<option value="'+ alamat +'">'+ alamat +'</option>');
                            });
                         }
                      });
                   }
                   else
                   {
                      $('select[name="dari_alamat"]').empty();
                   }
                });
    });
</script>

<script type="text/javascript">
    jQuery(document).ready(function ()
    {
                jQuery('select[name="tujuan_instansi"]').on('change',function(){
                   var id = jQuery(this).val();
                   console.log(id)

                //    alert(countryID);
                   if(id)
                   {
                      jQuery.ajax({
                         url : 'https://'+document.location.hostname+'/mail/public/getnama/'+id,
                         type : "GET",
                         dataType : "json",
                         cache: false,
                         success:function(data)
                         {
                            // alert(data);
                            console.log(data);
                            jQuery('select[name="tujuan_pengirim"]').empty();
                            $('select[name="tujuan_pengirim"]').append('<option value="'+ "" +'">'+ "Pilih Penerima" +'</option>');
                            jQuery.each(data, function(nama,alamat){
                                // console.log(nama);
                                // console.log(alamat);
                                $('select[name="tujuan_pengirim"]').append('<option value="'+ nama +'">'+ alamat + ' - ' + nama +'</option>');
                          });
                         }
                      });
                   }
                   else
                   {
                      $('select[name="tujuan_pengirim"]').empty();
                   }
                });

                jQuery('select[name="tujuan_instansi"]').on('change',function(){
                   var id = jQuery(this).val();
                   console.log(id)

                //    alert(id);
                   if(id)
                   {
                      jQuery.ajax({
                         url : 'https://'+document.location.hostname+'/mail/public/getalamat/'+id,
                         type : "GET",
                         dataType : "json",
                         cache: false,
                         success:function(data)
                         {
                            // alert(data);
                            console.log(data);
                            jQuery('select[name="tujuan_alamat"]').empty();
                            jQuery.each(data, function(nama,alamat){
                                // console.log(nama);
                                // console.log(alamat);
                                $('select[name="tujuan_alamat"]').append('<option value="'+ alamat +'">'+ alamat +'</option>');
                            });
                         }
                      });
                   }
                   else
                   {
                      $('select[name="tujuan_alamat"]').empty();
                   }
                });
    });
</script>
<script>
    function copier(e) {
    document.getElementById("kker").value = e.target.value
}
</script>
@endsection
