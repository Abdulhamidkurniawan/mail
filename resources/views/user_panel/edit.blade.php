@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">Edit User</div>
                            <div class="card-body">
                                    <form class="" action="{{ route('user_panel.update', $user)}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }} <!-- membuat patch PostController bisa dibaca -->
                                            <div class="form-group row">
                                                <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('Name') }}</label>

                                                <div class="col-md-6">
                                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{$user->name}}" required autofocus>

                                                    @if ($errors->has('name'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="username" class="col-md-3 col-form-label text-md-right">{{ __('Username') }}</label>

                                                <div class="col-md-6">
                                                    <input id="username" type="username" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{$user->username}}" required autofocus>

                                                    @if ($errors->has('username'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('username') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="password" class="col-md-3 col-form-label text-md-right">{{ __('Password') }}</label>

                                                <div class="col-md-6">
                                                    <input id="password" type="text" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="" placeholder="Password Baru" required>

                                                    @if ($errors->has('password'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="password-confirm" class="col-md-3 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                                <div class="col-md-6">
                                                    <input id="password-confirm" type="text" class="form-control" name="password_confirmation" value="" placeholder="Password Baru" required>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="dari"class="col-md-3 col-form-label text-md-right">Dari</label>
                                                    <div class="col-md-6">
                                                    <select name="instansi" id="instansi" class="form-control select2">
                                                        <option value="">Pilih Instansi</option>
                                                        @foreach ($works as $work)
                                                            <option value="{{$work->instansi}}">{{$work->instansi}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <button type="button" class="btn btn-success float-right mb-1" data-toggle="modal" data-target="#modalTambahBarang">Tambah</button>
                                            </div>

                                            <div class="form-group row">
                                                <label for="jabatan" class="col-md-3 col-form-label text-md-right">{{ __('Jabatan') }}</label>

                                                <div class="col-md-6">
                                                    <input id="jabatan" type="jabatan" class="form-control{{ $errors->has('jabatan') ? ' is-invalid' : '' }}" name="jabatan" value="{{$user->bagian}}" required>
                                                </div>
                                            </div>

                                            <div class="form-group row{{ $errors->has('Level') ? ' has-error ' : '' }}">
                                                <label class="col-md-3 col-form-label text-md-right">{{ __('Level') }}</label>

                                                <div class="col-md-6">
                                                    <select name="level" class="form-control">
                                                        @foreach ($jabatans as $jabatan)   <!-- $categories dari PostController -->
                                                        <option value="{{$jabatan->jabatan}}"
                                                        @if ($jabatan->jabatan === $user->jabatan)
                                                            selected
                                                        @endif
                                                        > {{$jabatan->jabatan}} </option>
                                                                @endforeach
                                                       </select>

                                                </div>
                                              </div>
                                            <div class="form-group row mb-0">
                                                <div class="col-md-6 offset-md-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        {{ __('Simpan') }}
                                                    </button>
                                                </div>
                                            </div>
                                            </form>
                            </div><div class="modal fade" id="modalTambahBarang" tabindex="-1" aria-labelledby="modalTambahBarang" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Tambah Pengirim / Penerima Baru</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                        <!--FORM TAMBAH BARANG-->
                                        <form method="POST" action="{{ route('pekerjaan.store') }}">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="tker" class="col-md-2 col-form-label text-md-right">Biodata</label>
                                                <div class="col-md-6">
                                                    <input id="tinstansi" type="text" class="form-control" name="tinstansi" value="" placeholder="Nama Instansi Baru" autofocus>
                                                </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="tker" class="col-md-2 col-form-label text-md-right"></label>
                                                <div class="col-md-6">
                                                    <textarea name="talamat" id="talamat" class="form-control" placeholder="Alamat">{{ old('talamat') }}</textarea>
                                                </div>

                                        </div>
                                        <div class="form-group row">
                                            <label for="tker" class="col-md-2 col-form-label text-md-right"></label>
                                                <div class="col-md-6">
                                                    <button type="submit" class="btn btn-primary">Tambah</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-body">
                                        <!--FORM TAMBAH BARANG-->
                                            <div class="form-group row">
                                                <label for="tker" class="col-md-2 col-form-label text-md-right">Ingin dihapus</label>
                                                <div class="col-md-6">
                                                    <select name="" onchange="copier(event)" class="form-control select2">
                                                        <option value="">Pilih Data</option>
                                                    @foreach ($datas as $data)
                                                        <option value="{{$data->id}}">{{$data->instansi}}</option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                                @if (!$datas->isEmpty())
                                                <form method="POST" action="{{ route('pekerjaan.destroy', $data->id) }}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <input id="kker" name="kker" type="hidden" value="">

                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                                {{-- <button type="button" onclick="myFunction()">Try it</button> --}}
                                                </form>
                                                @endif
                                        </div>
                                        </div>
                                    </div>

                                <!--END FORM TAMBAH BARANG-->
                    </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
          $(".btn-success").click(function(){
              var html = $(".clone").html();
              $(".increment").after(html);
          });
          $("body").on("click",".btn-danger",function(){
              $(this).parents(".control-group").remove();
          });
        });
    </script>
    <script>
        function copier(e) {
        document.getElementById("kker").value = e.target.value
    }
    </script>

@endsection
