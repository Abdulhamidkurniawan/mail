@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div class="card-body">
                    <form method="POST" action="{{ route('user_panel.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="nama" class="col-md-3 col-form-label text-md-right">{{ __('Nama') }}</label>

                            <div class="col-md-6">
                                <input id="nama" type="nama" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="nama" value="{{ old('nama') }}" required autocomplete="nama" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="username" class="col-md-3 col-form-label text-md-right">{{ __('Username') }}</label>

                            <div class="col-md-6">
                                <input id="username" type="username" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autocomplete="username">

                                @if ($errors->has('username'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-3 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-3 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="dari"class="col-md-3 col-form-label text-md-right">Dari</label>
                                <div class="col-md-6">
                                <select name="instansi" id="instansi" class="form-control select2">
                                    <option value="">Pilih Instansi</option>
                                    @foreach ($works as $work)
                                        <option value="{{$work->instansi}}">{{$work->instansi}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="button" class="btn btn-success float-right mb-1" data-toggle="modal" data-target="#modalTambahBarang">Tambah</button>
                          </div>

                          <div class="form-group row{{ $errors->has('Level') ? ' has-error ' : '' }}">
                            <label class="col-md-3 col-form-label text-md-right">{{ __('Jabatan') }}</label>

                            <div class="col-md-6">
                                <input id="jabatan" type="jabatan" class="form-control{{ $errors->has('jabatan') ? ' is-invalid' : '' }}" name="jabatan" value="{{ old('jabatan') }}" required autocomplete="jabatan">
                            </div>
                        </div>

                        <div class="form-group row{{ $errors->has('Level') ? ' has-error ' : '' }}">
                            <label class="col-md-3 col-form-label text-md-right">{{ __('Level') }}</label>

                            <div class="col-md-6">
                                <select name="level" id="level" class="form-control select2">
                                    <option value="">Pilih Jabatan</option>
                                    @foreach ($jabatans as $jabatan)
                                        <option value="{{$jabatan->jabatan}}">{{$jabatan->jabatan}}</option>
                                    @endforeach
                                </select>
                            </div>
                          </div>



                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="modal fade" id="modalTambahBarang" tabindex="-1" aria-labelledby="modalTambahBarang" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Tambah Pengirim / Penerima Baru</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                <!--FORM TAMBAH BARANG-->
                                <form method="POST" action="{{ route('pekerjaan.store') }}">
                                @csrf
                                <div class="form-group row">
                                    <label for="tker" class="col-md-2 col-form-label text-md-right">Biodata</label>
                                        <div class="col-md-6">
                                            <input id="tinstansi" type="text" class="form-control" name="tinstansi" value="" placeholder="Nama Instansi Baru" autofocus>
                                        </div>
                                </div>
                                <div class="form-group row">
                                    <label for="tker" class="col-md-2 col-form-label text-md-right"></label>
                                        <div class="col-md-6">
                                            <textarea name="talamat" id="talamat" class="form-control" placeholder="Alamat">{{ old('talamat') }}</textarea>
                                        </div>

                                </div>
                                <div class="form-group row">
                                    <label for="tker" class="col-md-2 col-form-label text-md-right"></label>
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-primary">Tambah</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-body">
                                <!--FORM TAMBAH BARANG-->
                                    <div class="form-group row">
                                        <label for="tker" class="col-md-2 col-form-label text-md-right">Ingin dihapus</label>
                                        <div class="col-md-6">
                                            <select name="" onchange="copier(event)" class="form-control select2">
                                                <option value="">Pilih Data</option>
                                            @foreach ($datas as $data)
                                                <option value="{{$data->id}}">{{$data->instansi}}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                        @if (!$datas->isEmpty())
                                        <form method="POST" action="{{ route('pekerjaan.destroy', $data->id) }}">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <input id="kker" name="kker" type="hidden" value="">

                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                        {{-- <button type="button" onclick="myFunction()">Try it</button> --}}
                                        </form>
                                        @endif
                                </div>
                                </div>
                            </div>

                        <!--END FORM TAMBAH BARANG-->
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
      $(".btn-success").click(function(){
          var html = $(".clone").html();
          $(".increment").after(html);
      });
      $("body").on("click",".btn-danger",function(){
          $(this).parents(".control-group").remove();
      });
    });
</script>
<script>
    function copier(e) {
    document.getElementById("kker").value = e.target.value
}
</script>
@endsection
