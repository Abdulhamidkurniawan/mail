@extends('layouts.dashboard')
@section('content')



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Report
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">

        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Small boxes (Stat box) -->
      <!-- <section class="content">
      <div class="row">
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Interactive Area Chart</h3>


            </div>
            <div class="box-body">
            <div id="chart"></div>
          </div>
          </div>

        </div> -->
      <div class="row">

<div class="box">

          <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Bar Chart</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="barChart" style="height:400px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
            <!-- /.box-header -->
            <div class="box-body">
                {{-- @foreach ($powers as $power)
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-purple"><i class="fa fa-code-fork"></i></span>

                    <div class="info-box-content">
                      <span class="info-box-text">Node {{$power['id_node']}}</span>
                      <span class="info-box-text">R:{{$power['fasa_r']}} S:{{$power['fasa_s']}} T:{{$power['fasa_t']}}</span>
                      <span class="info-box-number">Tiga Fasa (KWH):{{$power['tiga_fasa']}}</span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>

                @endforeach --}}
              {{-- <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id Node</th>
                  <th>Id Message</th>
                  <th>Fasa R</th>
                  <th>Fasa S</th>
                  <th>Fasa T</th>
                  <th>3 Fasa</th>
                  <th>Time</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($powers as $power)
                <tr>
                  <td>{{$power['id_node']}}</td>
                  <td>{{$power['id_message']}}</td>
                  <td>{{$power['fasa_r']}}</td>
                  <td>{{$power['fasa_s']}}</td>
                  <td>{{$power['fasa_t']}}</td>
                  <td>{{$power['tiga_fasa']}}</td>
                  <td>{{$power['created_at']}}</td>
                </tr>
                @endforeach

                </tbody>

              </table> --}}
            </div>
            <!-- /.box-body -->
          </div>
        <!-- right col -->
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2022 <a href="#"> </a>
  </footer>

  <!-- Control Sidebar -->

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
{{-- <script src="{{asset('bower_components/chart.js/Chart.js')}}"></script> --}}
<script src="{{asset('plugins/chart.js/Chart.min.js')}}"></script>

<!-- FastClick -->
<script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<script>
    $(function () {
      /* ChartJS
       * -------
       * Here we will create a few charts using ChartJS
       */

        var areaChartData = {
        labels  : <?php echo $graph_bulan; ?>,
        datasets: [
        {
          label               : 'Surat Keluar',
          backgroundColor     : 'rgba(255, 99, 132, 0.5)',
          borderColor         : 'rgba(255, 99, 132, 1)',
          pointRadius          : false,
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(255,99,132,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(255,99,132,1)',
          data                : <?php echo $graph_sk; ?>
        },
        {
          label               : 'Surat Masuk',
          backgroundColor     : 'rgba(210, 214, 222, 0.5)',
          borderColor         : 'rgba(210, 214, 222, 1)',
          pointRadius         : false,
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : <?php echo $graph_sm; ?>
        },
      ]
    }

    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $('#barChart').get(0).getContext('2d')
    var barChartData = $.extend(true, {}, areaChartData)
    var temp0 = areaChartData.datasets[0]
    var temp1 = areaChartData.datasets[1]
    barChartData.datasets[0] = temp1
    barChartData.datasets[1] = temp0

    var barChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      datasetFill             : false,
      scales:   {
        yAxes:  [{
            display: true,
            ticks:
                {
                beginAtZero: true,
                }
                }]
                },
    }

    new Chart(barChartCanvas, {
      type: 'bar',
      data: barChartData,
      options: barChartOptions
    })

    })
  </script>
@endsection
