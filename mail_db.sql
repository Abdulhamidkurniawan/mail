-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 08, 2022 at 02:14 PM
-- Server version: 5.7.33
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mail_db`
--
CREATE DATABASE IF NOT EXISTS `mail_db` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `mail_db`;

-- --------------------------------------------------------

--
-- Table structure for table `arsips`
--

CREATE TABLE `arsips` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_urut` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_surat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dari_instansi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dari_pengirim` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dari_alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan_instansi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan_pengirim` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan_alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isi` text COLLATE utf8mb4_unicode_ci,
  `petugas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tembusan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_surat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catatan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `historis`
--

CREATE TABLE `historis` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_urut` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_surat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dari_instansi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dari_pengirim` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dari_alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan_instansi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan_pengirim` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan_alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isi` text COLLATE utf8mb4_unicode_ci,
  `petugas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tembusan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_surat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catatan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `historis`
--

INSERT INTO `historis` (`id`, `no_urut`, `no_surat`, `nama`, `hp`, `hal`, `dari_instansi`, `dari_pengirim`, `dari_alamat`, `tujuan_instansi`, `tujuan_pengirim`, `tujuan_alamat`, `isi`, `petugas`, `tembusan`, `file`, `tanggal`, `tanggal_surat`, `status`, `catatan`, `created_at`, `updated_at`) VALUES
(1, NULL, '123/KEC/VIII/2022', NULL, '123123123123', 'Surat Percobaan', 'Kecamanatan Samarinda Ulu', 'Hendi Sucipto', 'Jl. Ir. H. Juanda No.5, Air Putih, Kec. Samarinda Ulu, Kota Samarinda, Kalimantan Timur 75124', 'Kelurahan Sidodadi', 'Abdillah', 'Jl. Dr. Sutomo No.32, Sidodadi, Kec. Samarinda Ulu, Kota Samarinda, Kalimantan Timur 75124', '<p>Tes isi surat percobaan</p>', 'Hendi Sucipto', NULL, '123KECVIII2022_Surat Percobaan_Kecamanatan Samarinda Ulu_Kelurahan Sidodadi_1659364655.docx', '01 Agustus 2022', '29 Juli 2022', 'Terkirim', NULL, '2022-08-01 06:37:35', '2022-08-01 06:37:35'),
(2, NULL, '123/KEC/VIII/2022', NULL, '123123123123', 'Surat Percobaan', 'Kelurahan Sidodadi', 'Abdillah', 'Jl. Dr. Sutomo No.32, Sidodadi, Kec. Samarinda Ulu, Kota Samarinda, Kalimantan Timur 75124', 'Kecamanatan Samarinda Ulu', 'Staf Administrasi', 'Jl. Ir. H. Juanda No.5, Air Putih, Kec. Samarinda Ulu, Kota Samarinda, Kalimantan Timur 75124', '<p>Tes isi surat percobaan</p>', 'Abdillah', NULL, '123KECVIII2022_Surat Percobaan_Kecamanatan Samarinda Ulu_Kelurahan Sidodadi_1659364655.docx', '01 Agustus 2022', '01 Agustus 2022', 'Terkirim', '<p>Surat sudah dilaksanakan, Terima kasih</p>', '2022-08-01 06:46:57', '2022-08-01 06:46:57'),
(3, NULL, '123/KEC/VIII/2022', NULL, '123123123123', 'Surat Percobaan', 'Kecamanatan Samarinda Ulu', 'Hendi Sucipto', 'Jl. Ir. H. Juanda No.5, Air Putih, Kec. Samarinda Ulu, Kota Samarinda, Kalimantan Timur 75124', 'Kelurahan Sidodadi', 'Abdillah', 'Jl. Dr. Sutomo No.32, Sidodadi, Kec. Samarinda Ulu, Kota Samarinda, Kalimantan Timur 75124', '<p>Tes isi surat percobaan</p>', 'Abdillah', NULL, '123KECVIII2022_Surat Percobaan_Kecamanatan Samarinda Ulu_Kelurahan Sidodadi_1659364655.docx', '01 Agustus 2022', '29 Juli 2022', 'Selesai', NULL, '2022-08-01 06:47:03', '2022-08-01 06:47:03'),
(4, NULL, '12', NULL, '8642363752', 'pengumuman', 'Kecamatan Samarinda Seberang', 'Tuti', 'Samarinda Seberang', 'Kelurahan Sungai Keledang', 'Hatta Mustafa', 'Sungai Keledang', NULL, 'Hendi Sucipto', NULL, '12_pengumuman_Kecamatan Samarinda Seberang_Kelurahan Sungai Keledang_1659367942.docx', '01 Agustus 2022', '01 Agustus 2022', 'Terkirim', NULL, '2022-08-01 07:32:22', '2022-08-01 07:32:22'),
(5, NULL, '12', NULL, '8642363752', 'pengumuman', 'Kecamatan Samarinda Seberang', 'Tuti', 'Samarinda Seberang', 'Kelurahan Sungai Keledang', 'Hatta Mustafa', 'Sungai Keledang', NULL, 'Hatta Mustafa', NULL, '12_pengumuman_Kecamatan Samarinda Seberang_Kelurahan Sungai Keledang_1659367942.docx', '01 Agustus 2022', '01 Agustus 2022', 'Selesai', NULL, '2022-08-01 07:33:28', '2022-08-01 07:33:28');

-- --------------------------------------------------------

--
-- Table structure for table `jabatans`
--

CREATE TABLE `jabatans` (
  `id` int(10) UNSIGNED NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jabatans`
--

INSERT INTO `jabatans` (`id`, `jabatan`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL),
(2, 'administrasi', NULL, NULL),
(3, 'user', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2019_11_12_154812_create_debits_table', 1),
(5, '2019_11_12_162907_create_pumps_table', 1),
(7, '2019_11_13_160955_create_waters_table', 1),
(8, '2020_03_30_135819_create_monitors_table', 1),
(9, '2020_05_12_144157_create_powers_table', 1),
(14, '2019_02_25_112049_create_articles_table', 4),
(15, '2019_02_25_114415_create_tags_table', 4),
(16, '2019_03_01_130823_create_categories_table', 4),
(17, '2019_03_01_131911_create_posts_table', 4),
(18, '2019_03_01_132215_create_comments_table', 4),
(19, '2019_03_07_145458_create_mails_table', 4),
(20, '2019_03_19_165935_create_borangs_table', 4),
(21, '2019_04_17_144710_create_alumnis_table', 4),
(22, '2019_02_25_114937_create_article_tag_table', 5),
(350, '2014_10_12_000000_create_users_table', 6),
(351, '2014_10_12_100000_create_password_resets_table', 6),
(352, '2019_08_19_000000_create_failed_jobs_table', 6),
(353, '2019_11_13_160912_create_jabatans_table', 6),
(354, '2020_10_09_093727_create_surats_table', 6),
(355, '2020_10_09_095008_create_pasiens_table', 6),
(356, '2020_10_09_095248_create_rms_table', 6),
(357, '2020_10_15_113502_create_works_table', 6),
(358, '2020_11_02_034924_create_services_table', 6),
(359, '2020_11_02_041055_create_receipts_table', 6),
(360, '2020_11_09_061840_create_officers_table', 6),
(361, '2021_02_22_132413_create_bills_table', 6),
(362, '2021_02_23_141742_create_tests_table', 6),
(363, '2022_04_23_160008_create_instansis_table', 6),
(364, '2022_04_23_160357_create_suratmasuks_table', 6),
(365, '2022_04_23_160409_create_suratkeluars_table', 6),
(366, '2022_04_23_160423_create_historis_table', 6),
(367, '2022_04_23_160435_create_arsips_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `receipts`
--

CREATE TABLE `receipts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terima_dari` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `layanan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jumlah` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `petugas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `suratkeluars`
--

CREATE TABLE `suratkeluars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_urut` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_surat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dari_instansi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dari_pengirim` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dari_alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan_instansi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan_pengirim` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan_alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isi` text COLLATE utf8mb4_unicode_ci,
  `petugas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tembusan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_surat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catatan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suratkeluars`
--

INSERT INTO `suratkeluars` (`id`, `no_urut`, `no_surat`, `nama`, `hp`, `hal`, `dari_instansi`, `dari_pengirim`, `dari_alamat`, `tujuan_instansi`, `tujuan_pengirim`, `tujuan_alamat`, `isi`, `petugas`, `tembusan`, `file`, `tanggal`, `tanggal_surat`, `status`, `catatan`, `created_at`, `updated_at`) VALUES
(1, NULL, '123/KEC/VIII/2022', NULL, '123123123123', 'Surat Percobaan', 'Kecamanatan Samarinda Ulu', 'Hendi Sucipto', 'Jl. Ir. H. Juanda No.5, Air Putih, Kec. Samarinda Ulu, Kota Samarinda, Kalimantan Timur 75124', 'Kelurahan Sidodadi', 'Abdillah', 'Jl. Dr. Sutomo No.32, Sidodadi, Kec. Samarinda Ulu, Kota Samarinda, Kalimantan Timur 75124', '<p>Tes isi surat percobaan</p>', 'Hendi Sucipto', NULL, '123KECVIII2022_Surat Percobaan_Kecamanatan Samarinda Ulu_Kelurahan Sidodadi_1659364655.docx', '01 Agustus 2022', '29 Juli 2022', 'Selesai-Abdillah-Kelurahan Sidodadi', NULL, '2022-08-01 06:37:35', '2022-08-01 06:47:03'),
(2, NULL, '123/KEC/VIII/2022', NULL, '123123123123', 'Surat Percobaan', 'Kelurahan Sidodadi', 'Abdillah', 'Jl. Dr. Sutomo No.32, Sidodadi, Kec. Samarinda Ulu, Kota Samarinda, Kalimantan Timur 75124', 'Kecamanatan Samarinda Ulu', 'Staf Administrasi', 'Jl. Ir. H. Juanda No.5, Air Putih, Kec. Samarinda Ulu, Kota Samarinda, Kalimantan Timur 75124', '<p>Tes isi surat percobaan</p>', 'Abdillah', NULL, '123KECVIII2022_Surat Percobaan_Kecamanatan Samarinda Ulu_Kelurahan Sidodadi_1659364655.docx', '01 Agustus 2022', '01 Agustus 2022', 'Terkirim', '<p>Surat sudah dilaksanakan, Terima kasih</p>', '2022-08-01 06:46:57', '2022-08-01 06:46:57'),
(3, NULL, '12', NULL, '8642363752', 'pengumuman', 'Kecamatan Samarinda Seberang', 'Tuti', 'Samarinda Seberang', 'Kelurahan Sungai Keledang', 'Hatta Mustafa', 'Sungai Keledang', NULL, 'Hendi Sucipto', NULL, '12_pengumuman_Kecamatan Samarinda Seberang_Kelurahan Sungai Keledang_1659367942.docx', '01 Agustus 2022', '01 Agustus 2022', 'Dibaca-user-Kelurahan Sungai Keledang', NULL, '2022-08-01 07:32:22', '2022-08-01 07:33:32');

-- --------------------------------------------------------

--
-- Table structure for table `suratmasuks`
--

CREATE TABLE `suratmasuks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_urut` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_surat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dari_instansi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dari_pengirim` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dari_alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan_instansi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan_pengirim` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan_alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isi` text COLLATE utf8mb4_unicode_ci,
  `petugas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tembusan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_surat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catatan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instansi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bagian` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subbagian` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `email_verified_at`, `password`, `instansi`, `jabatan`, `bagian`, `subbagian`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'admin@gmail.com', NULL, '$2y$10$x6bou/wKKHwoMFZy8lRKUetfTSIw3UoiSjqGMq56Ij9lvZ.1DLUhy', NULL, 'admin', NULL, NULL, NULL, '2022-06-30 16:00:00', '2022-06-30 16:00:00'),
(8, 'Hendi Sucipto', 'adminkec', NULL, '2022-08-01 06:13:25', '$2y$10$ANKOdZosJ3puyt22uN83duC0YWpezFqPAQNcWae9K0A./7FEKXAai', 'Kecamanatan Samarinda Ulu', 'administrasi', 'Staf Administrasi', NULL, 'zOQV487odjcDQAziYAXoAsyeQPPItuH5edCHy1oMR4FWL1TfcOeg57vPL4SD', '2022-08-01 06:13:25', '2022-08-01 06:13:25'),
(9, 'Sari', 'adminkel', NULL, '2022-08-01 06:15:06', '$2y$10$pMxh5jh.v8zcmu6SPj0Ay.11Qy1cQ10Z4.an7Ba8ciZUHHqoJ0M..', 'Kelurahan Sidodadi', 'Staf Administrasi', 'Staf Administrasi', NULL, NULL, '2022-08-01 06:15:06', '2022-08-01 07:22:37'),
(10, 'Hatta Mustafa', 'sungaikeledang', NULL, '2022-08-01 06:57:12', '$2y$10$PNSOpPF8PCghD2CKrLxD1u01n.rHvczWb9UBavVOm5ygRVGcrrZma', 'Kelurahan Sungai Keledang', 'user', 'User', NULL, NULL, '2022-08-01 06:57:12', '2022-08-01 06:57:12'),
(11, 'Tuti', 'tutikecamatan', NULL, '2022-08-01 07:30:56', '$2y$10$By/G4ZenrbaYwi/Nv6/I6u3UBds7cfXfTAd3nuGam/TdK1hXniuHG', 'Kecamatan Samarinda Seberang', 'user', 'staff', NULL, NULL, '2022-08-01 07:30:56', '2022-08-01 07:30:56');

-- --------------------------------------------------------

--
-- Table structure for table `works`
--

CREATE TABLE `works` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nik` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instansi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bagian` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subbagian` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `golongan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `works`
--

INSERT INTO `works` (`id`, `nama`, `nik`, `instansi`, `alamat`, `jabatan`, `bagian`, `subbagian`, `golongan`, `created_at`, `updated_at`) VALUES
(6, NULL, NULL, 'Kelurahan Sungai Keledang', 'Sungai Keledang', NULL, NULL, NULL, NULL, '2022-08-01 06:55:31', '2022-08-01 06:55:31'),
(7, NULL, NULL, 'Kecamatan Samarinda Seberang', 'Samarinda Seberang', NULL, NULL, NULL, NULL, '2022-08-01 06:56:30', '2022-08-01 06:56:30'),
(8, NULL, NULL, 'Kelurahan Baqa', 'Baqa', NULL, NULL, NULL, NULL, '2022-08-01 07:02:55', '2022-08-01 07:02:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `arsips`
--
ALTER TABLE `arsips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `historis`
--
ALTER TABLE `historis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jabatans`
--
ALTER TABLE `jabatans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `receipts`
--
ALTER TABLE `receipts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suratkeluars`
--
ALTER TABLE `suratkeluars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suratmasuks`
--
ALTER TABLE `suratmasuks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `works`
--
ALTER TABLE `works`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `arsips`
--
ALTER TABLE `arsips`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `historis`
--
ALTER TABLE `historis`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `jabatans`
--
ALTER TABLE `jabatans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=368;

--
-- AUTO_INCREMENT for table `receipts`
--
ALTER TABLE `receipts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suratkeluars`
--
ALTER TABLE `suratkeluars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `suratmasuks`
--
ALTER TABLE `suratmasuks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `works`
--
ALTER TABLE `works`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
