<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    protected $fillable = ['nama','nik','golongan','instansi','bagian','subbagian','jabatan','alamat']; /* yang bsa di isi */
}
