<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Officer;
use App\Suratkeluar;
use App\Histori;
use App\Work;
use Auth;
use DB;
use Redirect;

class SuratKeluarController extends Controller
{
    public function index()
    {
        if (Auth::user()->jabatan == 'karyawan' or Auth::user()->jabatan == 'administrasi' ){
            $sks = Suratkeluar::where('petugas', 'like', '%'.Auth::user()->name.'%')->latest()->get();}
        else if (Auth::user()->jabatan == 'user'){
            $sks = Suratkeluar::where('petugas', 'like', '%'.Auth::user()->name.'%')->latest()->get();}

        else{$sks = Suratkeluar::latest()->get();}        // dd($sks);

        // dd($sks);
        // return("oke");
        return view('sk.index', compact('sks')); /* kirim var */
    }

    public function create()
    {
        // return ("oke");
        $works = DB::select("SELECT * FROM works WHERE id IN (
            SELECT MAX(id)
            FROM works
            GROUP BY instansi
        )ORDER BY instansi ASC");
        $datas = Work::all();
        return view('sk.create', compact('works','datas')); /* kirim var */
    }

    public function store(Request $request)
    {
        function tgl_indo($tanggal){
            $bulan = array (
                1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            $pecahkan = explode('-', $tanggal);

            // variabel pecahkan 0 = tanggal
            // variabel pecahkan 1 = bulan
            // variabel pecahkan 2 = tahun

            return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
        }
        // dd(tgl_indo("2022-04-05"));
        // dd($request->All(),$request->file->getClientOriginalName(),$request->file->extension());
        if($request->hasfile('file'))
        {
            $request->validate([
                'file' => 'required|mimes:pdf,docx,doc|max:20480',
            ]);
            $no_surat = str_ireplace( array( '\'', '"', ',' , ';', '<', '>', '/' ), '', $request->no_surat);
            $fileName = $no_surat.'_'.$request->hal.'_'.$request->dari_instansi.'_'.$request->tujuan_instansi.'_'.time().'.'.$request->file->extension();
            $request->file->move(public_path('upload_surat'), $fileName);
        }
        // dd($fileName);
        if (Suratkeluar::where('no_surat', '=', $request->no_surat)->exists()) {
            return Redirect::back()->withErrors(['no_surat' => 'Nomor surat sudah ada !']);
         }
        Suratkeluar::create([
            'no_urut' => request('no_urut'),
            'no_surat' => request('no_surat'),
            'nama' => request('nama'),
            'hp' => request('hp'),
            'hal' => request('hal'),
            'dari_instansi' => request('dari_instansi'),
            'dari_pengirim' => request('dari_pengirim'),
            'dari_alamat' => request('dari_alamat'),
            'tujuan_instansi' => request('tujuan_instansi'),
            'tujuan_pengirim' => request('tujuan_pengirim'),
            'tujuan_alamat' => request('tujuan_alamat'),
            'isi' => request('isi'),
            'petugas' => Auth::user()->name,
            'tembusan' => request('tembusan'),
            'file' => $fileName,
            'tanggal' => tgl_indo(date("Y-m-d")),
            'tanggal_surat' => tgl_indo($request->tanggal),
            'status' => 'Terkirim',
        ]);

        Histori::create([
            'no_urut' => request('no_urut'),
            'no_surat' => request('no_surat'),
            'nama' => request('nama'),
            'hp' => request('hp'),
            'hal' => request('hal'),
            'dari_instansi' => request('dari_instansi'),
            'dari_pengirim' => request('dari_pengirim'),
            'dari_alamat' => request('dari_alamat'),
            'tujuan_instansi' => request('tujuan_instansi'),
            'tujuan_pengirim' => request('tujuan_pengirim'),
            'tujuan_alamat' => request('tujuan_alamat'),
            'isi' => request('isi'),
            'petugas' => Auth::user()->name,
            'tembusan' => request('tembusan'),
            'file' => $fileName,
            'tanggal' => tgl_indo(date("Y-m-d")),
            'tanggal_surat' => tgl_indo($request->tanggal),
            'status' => 'Terkirim',
        ]);

        // return ("oke");
        return redirect()->route('sk.index')->withSuccess('surat berhasil dibuat');
    }

    public function edit(SuratKeluar $sk)
    {
        return view('sk.edit', compact('sk'));
    }

    public function update(Request $request, SuratKeluar $sk)
    {
        // dd($request->all());
        $update = $request->All();
        $sk->update($update);
        return redirect()->route('sk.index')->withSuccess('data berhasil diubah');
    }

    public function destroy(SuratKeluar $sk)
    {
        $sk->delete();

        return redirect()->route('sk.index')->withDanger('data berhasil dihapus');
    }

    public function getnama($id)
    {
        $namas = DB::table("users")
        ->where("instansi",'like','%'.$id.'%')
        ->pluck("bagian","name");
        return json_encode($namas);
    }

    public function getalamat($id)
    {
        $namas = DB::table("works")
        ->where("instansi",'like','%'.$id.'%')
        ->pluck("alamat","nama");
        return json_encode($namas);
    }

}
