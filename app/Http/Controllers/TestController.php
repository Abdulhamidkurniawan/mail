<?php

namespace App\Http\Controllers;

use App\Work;
use App\Tests;
use Illuminate\Http\Request;
use DB;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tests = Tests::all();
        // dd($tests);
        $jeniss = DB::select("SELECT * FROM surats WHERE id IN (
            SELECT MAX(id)
            FROM surats
            GROUP BY jenis_rm
        )ORDER BY jenis_rm ASC");
        return view('test.index', compact('tests','jeniss')); /* kirim var */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Tests::create([
            'nama' => request('tnama'),
            'kode' => request('tkode'),
            'jenis' => request('tjenis'),
            'harga' => request('tharga'),

        ]);
        return redirect()->route('test.index')->withSuccess('data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        // dd($request->kker);
        // $work->delete();
        DB::delete('delete from tests where id = ?',[$request->ktest]);

        return redirect()->route('test.index')->withDanger('data berhasil dihapus');

    }
}
