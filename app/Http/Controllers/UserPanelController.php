<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Jabatan;
use App\Suratkeluar;
use App\Work;
use Auth;
use DB;
use Carbon\Carbon;

class UserPanelController extends Controller
{
    /* fungsi2 yang bisa dipanggil */
    public function index()
    {
        $users = User::latest()
        ->where('jabatan', '!=', 'admin')
        ->Paginate(10);

        return view('user_panel.index', compact('users')); /* kirim var */
    }
    public function create()
    {
        // $categories = Category::All();
        $works = DB::select("SELECT * FROM works WHERE id IN (
            SELECT MAX(id)
            FROM works
            GROUP BY instansi
        )ORDER BY instansi ASC");
        $jabatans = Jabatan::where('jabatan', '!=', 'admin')->get();
        $datas = Work::All();

        return view('user_panel.create', compact('works','jabatans','datas'));
    }

    public function store()
    {
        $this->validate(request(), [
            'username' => 'required|min:5'
        ]);
        User::create([
            'name' => request('nama'),
            'username' => request('username'),
            'instansi' => request('instansi'),
            'email' => null,
            'password' => bcrypt(request('password')),
            'bagian' => request('jabatan'),
            'jabatan' => request('level'),
            'email_verified_at' => Carbon::now(),
        ]);
        return redirect()->route('user_panel.index')->withSuccess('data berhasil ditambahkan');
    }

    // public function show(User $user){
    //     return view('user_panel.show', compact('post'));
    // }

    public function edit(User $user)  /* public function edit($id)   */
    {
        /* $post = Post::find($id); */
        $jabatans = Jabatan::All();

        $works = DB::select("SELECT * FROM works WHERE id IN (
            SELECT MAX(id)
            FROM works
            GROUP BY instansi
        )ORDER BY instansi ASC");
        $jabatans = Jabatan::where('jabatan', '!=', 'admin')->get();
        $datas = Work::All();

        return view('user_panel.edit', compact('user','works','jabatans','datas'));          /* $post dikirim view edit,dll*/
    }

    public function Update(User $user)
    {
        /*$post = Post::find($id);*/
        $user->update([
            'name' => request('name'),
            'email' => request('email'),
            'jabatan' => request('jabatan'),
            'password' => bcrypt(request('password')),
        ]);
        return redirect()->route('user_panel.index')->withInfo('data berhasil diubah');
    }
    public function destroy(User $user)  /* public function edit($id)   */
    {
        $user->delete();

        return redirect()->route('user_panel.index')->withDanger('data berhasil dihapus');
    }
}
