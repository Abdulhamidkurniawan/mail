<?php

namespace App\Http\Controllers;

use App\Work;
use Illuminate\Http\Request;
use DB;

class WorksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Work::create([
            'nama' => request('tnama'),
            'nik' => request('tnip'),
            'instansi' => request('tinstansi'),
            'alamat' => request('talamat'),
            'jabatan' => request('jabatan'),
            'bagian' => request('bagian'),
            'subbagian' => request('subbagian'),
            'golongan' => request('golongan'),
        ]);
        // return redirect()->route('sk.create')->withSuccess('data berhasil ditambahkan');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function show(Work $work)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function edit(Work $work)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Work $work)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Work $work)
    {
        // dd($request->kker);
        // $work->delete();
        DB::delete('delete from works where id = ?',[$request->kker]);

        // return redirect()->route('sk.create')->withDanger('data berhasil dihapus');
        return back();

    }
}
