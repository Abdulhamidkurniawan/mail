<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Pasien;
use App\Rm;
use App\Surat;
use App\Suratkeluar;
use Auth;
use Artisan;
use Illuminate\Support\Facades\Route;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return view('home');
    }

    public function dashboard()
    {
        if (Auth::user()->jabatan == 'karyawan' or Auth::user()->jabatan == 'administrasi' ){
            $sks = Suratkeluar::where('petugas', 'like', '%'.Auth::user()->name.'%')->latest()->get();}
        else if (Auth::user()->jabatan == 'user'){
            $sks = Suratkeluar::where('petugas', 'like', '%'.Auth::user()->name.'%')
            ->latest()->get();}

            // $sks = Suratkeluar::where('petugas', 'like', '%'.Auth::user()->name.'%')
            // ->where('status', 'like', '%'.'Terkirim'.'%')
            // ->latest()->get();}
        else{$sks = Suratkeluar::latest()->get();}
        // $sks = Suratkeluar::where('status', 'like', '%'.'Terkirim'.'%')->latest()->get();
        $valsks = $sks->count();

        if (Auth::user()->jabatan == 'karyawan' or Auth::user()->jabatan == 'administrasi' ){
            $sms = Suratkeluar::where('tujuan_instansi', 'like', '%'.Auth::user()->instansi.'%')
            ->where('petugas', '!=', Auth::user()->name)
            ->latest()->get();}
        else if (Auth::user()->jabatan == 'user'){
            $sms = Suratkeluar::where('tujuan_instansi', 'like', '%'.Auth::user()->instansi.'%')
            ->latest()->get();}
        else{$sms = Suratkeluar::latest()->get();}
        $valsms = $sms->count();

        $users = User::where('jabatan', '!=', 'admin')->get();
        $valu = $users->count();
        $name = Route::currentRouteName();
        // dd($name);
        return view('dashboard', compact('valu','valsks','valsms','name'));
    }

    public function optimize()
    {
        $exitCode = Artisan::call('optimize');
        return '<h1>Reoptimized class loader</h1>';    }

}
