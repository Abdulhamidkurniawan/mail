<?php

namespace App\Http\Controllers;

use App\Histori;
use Illuminate\Http\Request;
use App\Suratkeluar;

class RedirectController extends Controller
{
    public function default()
    {
        return view('welcome');
    }

    public function cari()
    {
        return view('cari');
    }

    public function cek(Request $request)
    {
        $hasils = Histori::where('no_surat', 'like', '%'.$request->cari.'%')->get();
        // dd($hasils);
        if(!$hasils->isEmpty()) {
            // dd($ktp,$hasil);
            return view('hasil', compact('hasils'));
        }
        return view('cari');
    }

    public function hasil()
    {
        return view('hasil');
    }

    public function report(Request $request)
    {
        $surats = Suratkeluar::whereYear('created_at', '=', $request->tahun)->get();
        // $bulans = DB::select("SELECT * FROM suratkeluars WHERE id IN (
        //     SELECT MAX(id)
        //     FROM suratkeluars
        //     GROUP BY instansi
        // )ORDER BY instansi ASC");
        $hitung_surat = 0;
        if (count($surats) > 0){
            foreach ($surats as $bulan){
                $pecahkan = explode(' ', $bulan->tanggal_surat);
                $graph_bulan[]=$pecahkan[1];
                $graph_bulan=array_unique($graph_bulan);
                $bln[]= $bulan->tanggal_surat;
                $hitung_surat=$hitung_surat+1;
            }
            foreach ($graph_bulan as $filter){
                $sk_filter = Suratkeluar::whereYear('created_at', '=', $request->tahun)
                ->where('tanggal_surat', 'like', '%'.$filter.'%')
                ->where('tujuan_instansi', 'like', '%'.'kelurahan'.'%')
                ->get();
                $count_sk[] = $sk_filter->count();

                $sm_filter = Suratkeluar::whereYear('created_at', '=', $request->tahun)
                ->where('tanggal_surat', 'like', '%'.$filter.'%')
                ->where('tujuan_instansi', 'like', '%'.'kecamatan'.'%')
                ->get();
                $count_sm[] = $sm_filter->count();
            }
            // foreach ($surats as $prst){
            //     $graph_angka[]=$prst->tiga_fasa;
            // }
            $graph_bulan = json_encode($graph_bulan);
            $graph_sk = json_encode($count_sk);
            $graph_sm = json_encode($count_sm);
            }

        else{
            return "Tidak ada data";
            }

        // dd($surats,$graph_bulan,$bln,$hitung_surat,$surat_filter,$count_surat,$graph_surat);
        return view('report', compact('graph_bulan','graph_sk','graph_sm')); /* kirim var */
    }
}
