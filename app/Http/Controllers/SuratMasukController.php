<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Officer;
use App\Suratkeluar;
use App\Suratmasuk;
use App\Histori;
use App\Work;
use Auth;
use DB;

class SuratMasukController extends Controller
{
    public function index()
    {

        if (Auth::user()->jabatan == 'karyawan' or Auth::user()->jabatan == 'administrasi'){
            $sks = Suratkeluar::where('tujuan_instansi', 'like', '%'.Auth::user()->instansi.'%')
            ->where('petugas', '!=', Auth::user()->name)
            ->latest()->get();}
        else if (Auth::user()->jabatan == 'user'){
            $sks = Suratkeluar::where('tujuan_instansi', 'like', '%'.Auth::user()->instansi.'%')
            ->latest()->get();}
        else {$sks = Suratkeluar::latest()->get();}
        // dd(Auth::user()->instansi,$sks);
        // return("oke");
        return view('sm.index', compact('sks')); /* kirim var */
    }

    public function show(Suratkeluar $sk)
    {
        function tgl_indo($tanggal){
            $bulan = array (
                1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            $pecahkan = explode('-', $tanggal);

            // variabel pecahkan 0 = tanggal
            // variabel pecahkan 1 = bulan
            // variabel pecahkan 2 = tahun

            return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
        }
        if(substr($sk->status, 0, 10) != 'Diteruskan'){
        $sk->update([
            'status' => 'Dibaca'.'-'.Auth::user()->jabatan.'-'.Auth::user()->instansi,
        ]);
        }
        if (substr($sk->status, 0, 6) != 'Dibaca'){
        Histori::create([
            'no_urut' => $sk->no_urut,
            'no_surat' => $sk->no_surat,
            'nama' => $sk->nama,
            'hp' => $sk->hp,
            'hal' => $sk->hal,
            'dari_instansi' => $sk->dari_instansi,
            'dari_pengirim' => $sk->dari_pengirim,
            'dari_alamat' => $sk->dari_alamat,
            'tujuan_instansi' => $sk->tujuan_instansi,
            'tujuan_pengirim' => $sk->tujuan_pengirim,
            'tujuan_alamat' => $sk->tujuan_alamat,
            'isi' => $sk->isi,
            'petugas' => Auth::user()->name,
            'tembusan' => $sk->tembusan,
            'file' => $sk->file,
            'tanggal' => tgl_indo(date("Y-m-d")),
            'tanggal_surat' => $sk->tanggal_surat,
            'status' => $sk->status,
        ]);
        }
        return view('sm.show', compact('sk'));
    }

    public function forward(Suratkeluar $sk)
    {
        function tgl_indo($tanggal){
            $bulan = array (
                1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            $pecahkan = explode('-', $tanggal);

            // variabel pecahkan 0 = tanggal
            // variabel pecahkan 1 = bulan
            // variabel pecahkan 2 = tahun

            return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
        }

        $works = DB::select("SELECT * FROM works WHERE id IN (
            SELECT MAX(id)
            FROM works
            GROUP BY instansi
        )ORDER BY instansi ASC");
        $datas = Work::all();

        return view('sm.forward', compact('sk','works','datas'));
    }

    public function store(Request $request)
    {
        function tgl_indo($tanggal){
            $bulan = array (
                1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            $pecahkan = explode('-', $tanggal);

            // variabel pecahkan 0 = tanggal
            // variabel pecahkan 1 = bulan
            // variabel pecahkan 2 = tahun

            return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
        }
        // dd(tgl_indo("2022-04-05"));
        // dd($request->All());
        // dd($fileName);

        Suratkeluar::create([
            'no_urut' => request('no_urut'),
            'no_surat' => request('no_surat'),
            'nama' => request('nama'),
            'hp' => request('hp'),
            'hal' => request('hal'),
            'dari_instansi' => request('dari_instansi'),
            'dari_pengirim' => request('dari_pengirim'),
            'dari_alamat' => request('dari_alamat'),
            'tujuan_instansi' => request('tujuan_instansi'),
            'tujuan_pengirim' => request('tujuan_pengirim'),
            'tujuan_alamat' => request('tujuan_alamat'),
            'isi' => request('isi'),
            'petugas' => Auth::user()->name,
            'tembusan' => request('tembusan'),
            'file' => request('file'),
            'tanggal' => tgl_indo(date("Y-m-d")),
            'tanggal_surat' => request('tanggal'),
            'status' => 'Terkirim',
            'catatan' => request('catatan'),

        ]);

        Histori::create([
            'no_urut' => request('no_urut'),
            'no_surat' => request('no_surat'),
            'nama' => request('nama'),
            'hp' => request('hp'),
            'hal' => request('hal'),
            'dari_instansi' => request('dari_instansi'),
            'dari_pengirim' => request('dari_pengirim'),
            'dari_alamat' => request('dari_alamat'),
            'tujuan_instansi' => request('tujuan_instansi'),
            'tujuan_pengirim' => request('tujuan_pengirim'),
            'tujuan_alamat' => request('tujuan_alamat'),
            'isi' => request('isi'),
            'petugas' => Auth::user()->name,
            'tembusan' => request('tembusan'),
            'file' => request('file'),
            'tanggal' => tgl_indo(date("Y-m-d")),
            'tanggal_surat' => request('tanggal'),
            'status' => 'Terkirim',
            'catatan' => request('catatan'),
        ]);
        // dd($request->All());

        // return ("oke");
        return redirect()->route('sm.index')->withSuccess('surat berhasil dibuat');
    }

    public function selesai(Suratkeluar $sk)
    {
        function tgl_indo($tanggal){
            $bulan = array (
                1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            $pecahkan = explode('-', $tanggal);

            // variabel pecahkan 0 = tanggal
            // variabel pecahkan 1 = bulan
            // variabel pecahkan 2 = tahun

            return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
        }

        if (substr($sk->status, 0, 7) != 'Selesai'){
            Histori::create([
                'no_urut' => $sk->no_urut,
                'no_surat' => $sk->no_surat,
                'nama' => $sk->nama,
                'hp' => $sk->hp,
                'hal' => $sk->hal,
                'dari_instansi' => $sk->dari_instansi,
                'dari_pengirim' => $sk->dari_pengirim,
                'dari_alamat' => $sk->dari_alamat,
                'tujuan_instansi' => $sk->tujuan_instansi,
                'tujuan_pengirim' => $sk->tujuan_pengirim,
                'tujuan_alamat' => $sk->tujuan_alamat,
                'isi' => $sk->isi,
                'petugas' => Auth::user()->name,
                'tembusan' => $sk->tembusan,
                'file' => $sk->file,
                'tanggal' => tgl_indo(date("Y-m-d")),
                'tanggal_surat' => $sk->tanggal_surat,
                'status' => 'Selesai',
                'catatan' => $sk->catatan,
            ]);
            }

        $sk->update([
            'status' => 'Selesai'.'-'.Auth::user()->name.'-'.Auth::user()->instansi,
        ]);

        return redirect()->route('sm.index')->withSuccess('surat berhasil dibuat');
    }

    public function diarsipkan(Request $request, Suratkeluar $sk)
    {
        dd($request->all());
    }

}
