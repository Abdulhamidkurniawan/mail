<?php
namespace App\Exports;

use App\User;
use App\Rm;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;

class UserReport implements FromCollection
{
    public function collection()
    {
        return User::all();
    }
}