<?php

namespace App\Exports;

use App\Rm;
use App\Surat;
use App\Officer;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

use PhpOffice\PhpSpreadsheet\Cell\Cell;
use Maatwebsite\Excel\Concerns\ToModel;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;

use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class RmOfficers implements FromView, WithColumnFormatting, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    // public function collection()
    // {
    //     return Rm::all();
    // }

    use Exportable;

    public function columnFormats(): array
    {
        return [
            'F' => NumberFormat::FORMAT_NUMBER,
        ];

    }

    public function registerEvents() : array
    {
        return [
            BeforeExport::class  => function(BeforeExport $event) {
                $event->writer->setCreator('Klinik Bunga Bakung');
            },
            AfterSheet::class => function(AfterSheet $event) {
                $cellRange = 'A5:F5 '; // All headers

                $styleArray = [
                    'font' => ['bold' => true],
                ];

                $event->sheet->getStyle('A5:Z5')->applyFromArray($styleArray);
                // $event->sheet->getStyle('A15:Z15')->applyFromArray($styleArray);
                // $event->sheet->getStyle('A17:Z17')->applyFromArray($styleArray);
                $event->sheet->getStyle('A1:A4')->applyFromArray($styleArray);

            },
        ];
    }

    public function forYear($year)
    {
        $this->year = $year;

        return $this;
    }

    public function view(): View
    {

        function tgl_indo($tanggal){
            $bulan = array (
                1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            $pecahkan = explode('-', $tanggal);

            // variabel pecahkan 0 = tanggal
            // variabel pecahkan 1 = bulan
            // variabel pecahkan 2 = tahun

            return $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
        }
        // dd($tgl,date('Y-m'),tgl_indo($tgl));
        $tgl = tgl_indo($this->year);
        // $bln = substr($tgl,3);
        $bulan = $this->year;
        $rms = Surat::where('tanggal', 'like', '%'.$this->year.'%')->get();
        $ttl = $rms->count();
        return view('rm.excel', compact('rms','tgl','bulan'));

    }
}
