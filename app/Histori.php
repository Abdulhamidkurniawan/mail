<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Histori extends Model
{
    protected $fillable = ['no_urut','no_surat','nama','hp','hal','dari_instansi','dari_pengirim','dari_alamat','tujuan_instansi','tujuan_pengirim','tujuan_alamat','isi','petugas','tembusan','file','tanggal','tanggal_surat','status','catatan']; /* yang bsa di isi */
}
